/*
 server9: A 9P2000 server
 Copyright (C) 2019-2021  Martijn Heil

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::convert::TryFrom;
use nine::p2000 as np2000;

pub struct ConversionError();

pub struct FileMode(pub vfs9::FileMode);
impl From<FileMode> for np2000::FileMode {
    fn from(m: FileMode) -> Self {
        np2000::FileMode::from_bits(m.0.to_bits()).unwrap()
    }
}

pub struct FileType(pub vfs9::FileType);
impl From<FileType> for np2000::FileType {
    fn from(t: FileType) -> Self {
        np2000::FileType::from_bits(t.0.to_bits()).unwrap()
    }
}

pub struct Qid(pub vfs9::Qid);
impl From<Qid> for np2000::Qid {
    fn from(q: Qid) -> Self {
        let o = q.0;
        Self {
            file_type: FileType(o.file_type).into(),
            version: o.version,
            path: o.path
        }
    }
}

pub struct Stat(pub vfs9::Stat);
impl From<Stat> for np2000::Stat {
    fn from(s: Stat) -> Self {
        let o = s.0;

        np2000::Stat {
            type_: o.type_,
            dev: o.dev,
            qid: Qid(o.qid).into(),
            mode: FileMode(o.mode).into(),
            atime: o.atime,
            mtime: o.mtime,
            length: o.length,
            name: o.name.into(),
            uid: o.uid.into(),
            gid: o.gid.into(),
            muid: o.muid.into()
        }
    }
}
