/*
 server9: A 9P2000 server
 Copyright (C) 2019-2021  Martijn Heil

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


extern crate byteorder;
extern crate nine;
extern crate serde;
extern crate byte_unit;

use std::net::TcpStream;
use std::net::SocketAddr;
use std::net::TcpListener;

use std::io::{Read, Write};

use std::thread;

use std::clone::Clone;

use std::collections::HashMap;

use serde::Deserialize;

use nine::de::*;
use nine::p2000::*;

use byteorder::{ReadBytesExt, WriteBytesExt, BigEndian, LittleEndian};
use byte_unit::*;

use vfs9::*;

mod conversions;

/*
 * For documentation on 9P2000 refer to http://ericvh.github.io/9p-rfc/rfc9p2000.html
 */

//fn main() {
//  let port = 9284; // randomly chosen by smashing on keyboard
//  std::process::exit(match run_app(port) {
//    Err(err) => {
//      eprintln!("Error: {}", err);
//      1
//    }
//    Ok(_) => 0
//  })
//}

fn read_msgheader<T: Read>(stream: &mut T) -> Result<(u32, u8), Box<dyn std::error::Error>> {
  let msglen: u32 = stream.read_u32::<LittleEndian>()?;
  msglen -= 4;
  let msgtype: u8 = stream.read_u8()?;
}

// TODO better error handling everywhere
fn handle_client<F: File, D: Directory<F>>(client: TcpStream, max_msg_len: u32, root: D) -> Result<(), Box<dyn std::error::Error>> {
  let stream = client;
  let saddr = client.peer_addr().unwrap();
  println!("Client connected from {}", saddr);

  let des = nine::de::ReadDeserializer(stream.try_clone().unwrap());
  let ser = nine::ser::WriteSerializer::new(stream.try_clone().unwrap());

  // Fist request from the client should be a Tversion
  let (version_msglen, version_msgtype) = read_msgheader(&mut stream)?;
  if version_msgtype != Tversion::MSG_TYPE_ID {
    panic!("First request received by client from {} is not of type Tversion.", saddr);
  }
  let tversion: Tversion = Deserialize::deserialize(&mut des)?;
  let msgsize = std::cmp::min(max_msg_len, tversion.msize);
  println!("Received Tversion from {} Requested message size was {:.2}. Using message size of {:.2}",
      saddr, Byte::from_bytes(tversion.msize.into()).get_appropriate_unit(true), msgsize);
  Rversion {
    tag: NOTAG,
    msize: msgsize,
    version: "9P2000".into(),
  }.serialize(&mut ser).unwrap();

  let mut cwd = &root;
  let mut fidmap_dirs: HashMap<u32, D> = HashMap::new();
  let mut fidmap_files: HashMap<u32, F> = HashMap::new();


  let (attach_msglen, attach_msgtype) = read_msgheader(&mut stream);
  if attach_msgtype != Tattach::MSG_TYPE_ID {
    panic!("Expected Tattach. Instead we got a message with type id {}", attach_msgtype);
  }
  let tattach: Tattach = Deserialize::deserialize(&mut des)?;
  let provided_username = tattach.uname;
  let attachpoint = cwd.walk(&tattach.aname).unwrap();
  match attachpoint {
    DirectoryOrFile::File(f) => { panic!("Attach point is a file!"); }
    DirectoryOrFile::Directory(d) => {
      cwd = &d;
      fidmap_dirs.insert(tattach.afid, d);
      // TODO write Rattach
    }
  }




  loop {
    // 9P uses little endian as the network byte order.. hurray for sane design!
    let msglen: u32 = stream.read_u32::<LittleEndian>()?;
    msglen -= 4; // The sent prefixed message length includes the 4 byte long u32 length prefix.
    let msgtype: u8 = stream.read_u8()?; // Endianness of course doesn't matter for a u8, but this is nice anyway.

    // T-messages are serverbound requests and R-messages are clientbound responses.
    match msgtype {
      t if Tattach::MSG_TYPE_ID == t => {
        unimplemented!();
      },

      t if Tauth::MSG_TYPE_ID == t => {
        unimplemented!();
      },

      t if Tclunk::MSG_TYPE_ID == t => {
        let tclunk: Tclunk = Deserialize::deserialize(&mut des)?;
        fidmap_files.remove(tclunk.fid);
        fidmap_dirs.remove(tclunk.fid);
        Rclunk {
          tag: tclunk.tag,
        }.serialize(&mut ser).unwrap();
      },

      t if Tcreate::MSG_TYPE_ID == t => {
        let tcreate: Tcreate = Dersialize::deserialize(&mut des)?;

        match fidmap_dirs.get_mut() {
            Some(dir) => {
                dir.create_file(tcreate.name, Permissions::from_bitmask(tcreate.perm));
            }
            None => { /* TODO handle error */ }
        }
      },

      t if Tflush::MSG_TYPE_ID == t => {
        // TODO VFS9 will have to be changed to implement this
      },

      t if Topen::MSG_TYPE_ID == t => {
        let topen: Topen = Deserialize::deserialize(&mut des)?;
        match fidmap_files.get_mut(&topen.fid) {
            Some(file) => {
                let result: (vfs9::Qid, IoUnit) = file.open(vfs9::OpenMode::from_bits(topen.mode.bits()).unwrap()).unwrap();
                let qid: Qid = result.0;
                let iounit: IoUnit = result.1;

                Ropen {
                    tag: topen.tag,
                    qid: qid.into(),
                    iounit: iounit as u32
                }.serialize(&mut ser).unwrap();
            }

            None => { /* TODO handle error */ }
        }

      },

      t if Tread::MSG_TYPE_ID == t => {

      },

      t if Tremove::MSG_TYPE_ID == t => {
        let tremove: Tremove = Deserialize::deserialize(&mut des)?;

        if let Some(file) = fidmap_files.get_mut(&tremove.fid) {
          file.remove(); // TODO handle error
        } else if let Some(dir) = fidmap_dirs.get_mut(&tremove.fid) {
          dir.remove(); // TODO handle error
        }

        fidmap_dirs.remove(tremove.fid);
      },

      t if Tstat::MSG_TYPE_ID == t => {
        let tstat: Tstat = Deserialize::deserialize(&mut des)?;

        let file = fidmap_files.get(&tstat.fid);
        let dir = fidmap_files.get(&tstat.fid);
        let result: Result<vfs9::Stat, vfs9::Vfs9Error> = {
            if let Some(f) = file {
                f.stat()
            } else if let Some(d) = dir {
                d.stat()
            } else {
                unimplemented!()
                // TODO handle error
            }
        };

        // TODO dont unwrap result
        Rstat {
            tag: tstat.tag,
            stat: conversions::Stat(result.unwrap()).into()
        }.serialize(&mut ser).unwrap();
      },

      t if Tversion::MSG_TYPE_ID == t => {
        panic!("Unexpected Tversion.");
      },

      // TODO check implementation with standard
      t if Twalk::MSG_TYPE_ID == t => {
        let twalk: Twalk = Deserialize::deserialize(&mut des)?;
        if twalk.wname.is_empty() {
          let file = fidmap_files.get(&twalk.fid);
          let dir = fidmap_dirs.get(&twalk.fid);

          if let Some(f) = file {
            let file = fidmap_files.remove(&twalk.fid).unwrap(); // legitimate unwrap
            fidmap_files.insert(twalk.newfid, file);
          } else if let Some(f) = dir {

          }
        } else {
          let mut dir = fidmap_dirs.get(&twalk.fid).unwrap().clone();
          for name in twalk.wname {
            dir = dir.walk(name);
            if let Err(_) = dir { panic!(); }
          }
          fidmap_dirs.insert(twalk.newfid, dir);

        }
      },

      t if Twrite::MSG_TYPE_ID == t => {

      },

      t if Twstat::MSG_TYPE_ID == t => {

      },
    }
  }
}

fn launch<F: File, D: Directory<F> + Clone + Send + 'static> (port: u16, root: D) -> Result<(), Box<dyn std::error::Error>> {
  let listener = TcpListener::bind(format!("127.0.0.1:{}", &port))?;
  println!("9P2000 server listening on 127.0.0.1:{}", &port);

  // Accept new connections
  for client in listener.incoming() {
    let rootdir = root.clone();
    thread::spawn(move || {
      handle_client(client.unwrap(), n_mib_bytes!(256, u32) as u32, rootdir);
    });
  }
  Ok(())
}
